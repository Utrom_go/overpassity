# overpassity
promise-based overpass api client in typescript

## api
### overpass(query, opt = {})

* `query`: *string* Overpass API query
* `opt`: *object* Query options
  * `endpoint` *string* Overpass API endpoint URL
  * `rateLimitRetries` *number* Number of retries when rate limited/gateway timeout-ed before giving up
  * `rateLimitPause` *number* Pause in between receiving a rate limited response and initiating a retry
  * `verbose` *boolean* Output verbose query information
  * `stream` *boolean* Return a stream.Readable (in Node) or ReadableStream (in browser)
  * `fetchOpts` *object* Options to be passed to fetch
  
Returns:
* If query is `[out:json]`, API response as JSON object
* If query is `[out:xml]` or `[out:csv]`, API response as string
* If  `opt.stream = true`, return `stream.Readable` (nodejs) / `ReadableStream` (browser)

## example
```ts
import type { OverpassJson } from "overpassity";
import { overpass } from "overpassity";

// json request
overpass(`node(787087087);[out:json];out ids;`).then((json) => {
  json = json as OverpassJson;
  assert.deepStrictEqual(json.elements[0], {
    type: "node",
    id: 787087087,
  });
});
```
